def is_number(s):
    try:
        if len(s)>0 and s[-1]=='%':
            float(s[0:-1])
        else:
            float(s)
        return True
    except ValueError:
        return False

def is_integer(s):
    try:
        if len(s)>0 and s[-1]=='%':
            int(s[0:-1])
        else:
            int(s)
        return True
    except ValueError:
        return False

def split_question_mark(sentence, ss, ss_pos):
    ss1 = []
    ss1_pos = []
    for s1, s1_pos in zip(ss, ss_pos):
            sz1 = s1.split('?')
            sz2 = []
            sz2_pos = []
            start_pos = s1_pos[0]

            if len(sz1[0])>0:
                sz2 = [sz1[0]]
                sz2_pos = [[start_pos, start_pos+len(sz1[0])]]
                start_pos+=len(sz1[0])

            for i in range(1,len(sz1)):
                if len(sz2)>0 and sz2[-1][0]=='?':
                    # consecutive ?
                    sz2[-1]+='?'
                    sz2_pos[-1][1]+=1
                    start_pos+=1
                else:
                    sz2.append('?')
                    sz2_pos.append([start_pos,start_pos+1])
                    start_pos+=1

                if len(sz1[i])>0:
                    sz2.append(sz1[i])
                    sz2_pos.append([start_pos,start_pos+len(sz1[i])])
                    start_pos+=len(sz1[i])
            ss1+=sz2
            ss1_pos+=sz2_pos
    return ss1, ss1_pos

def split_percent_sign(sentence, ss, ss_pos):
    ss1 = []
    ss1_pos = []
    for s1, s1_pos in zip(ss, ss_pos):
        sz1 = s1.split('%')
        sz2 = []
        sz2_pos = []
        start_pos = s1_pos[0]

        for i in range(len(sz1)):
            if len(sz1[i:])>1 and is_number(sz1[i]):
                sz2.append(sz1[i]+'%')
                sz2_pos.append([start_pos, start_pos+len(sz2[-1])])
                start_pos+=len(sz2[-1])
            elif len(sz1[i])>0:
                sz2.append(sz1[i])
                sz2_pos.append([start_pos, start_pos+len(sz2[-1])])
                start_pos+=len(sz2[-1])+1
            else:
                start_pos+=1
        ss1+=sz2
        ss1_pos+=sz2_pos
    return ss1, ss1_pos

def split_period_mark(sentence, ss, ss_pos):
    ss1 = []
    ss1_pos = []
    for s1, s1_pos in zip(ss, ss_pos):
        sz1 = s1.split('.')
        sz2 = []
        sz2_pos = []
        if is_number(s1) or len(sz1)==1: # number
            sz2 = [s1]
            sz2_pos = [s1_pos]
        elif len(sz1)>1: # dot sperated abbr
            s2 = sz1[0]
            start_pos = s1_pos[0]
            for i in range(1,len(sz1)):
                if len(sz1[i-1])==1 and len(sz1[i])==1:
                    s2+='.'+sz1[i]
                else:
                    sz2.append(s2)
                    sz2_pos.append([start_pos, start_pos+len(s2)])
                    start_pos+=len(s2)+1
                    s2 = sz1[i]
            if len(s2)>0:
                sz2.append(s2)
                sz2_pos.append([start_pos, start_pos+len(s2)])
        ss1+=sz2
        ss1_pos+=sz2_pos
    return ss1, ss1_pos

def split_foward_slash(sentence, ss, ss_pos):
    ss1 = []
    ss1_pos = []
    for s1, s1_pos in zip(ss, ss_pos):
        sz2 = []
        sz2_pos = []

        sz1 = s1.split('/')
        if len(sz1)==2 and (
            all([len(s2)==1 for s2 in sz1]) or
            all([is_integer(s2) for s2 in sz1])):
            sz2=[s1]
            sz2_pos = [s1_pos]
        else:
            start_pos = s1_pos[0]
            for i in range(len(sz1)):
                if len(sz1[i])>0:
                    sz2.append(sz1[i])
                    sz2_pos.append([start_pos, start_pos+len(sz1[i])])
                    start_pos+=len(sz1[i])+1
        ss1+=sz2
        ss1_pos+=sz2_pos
    return ss1, ss1_pos

def split_token(token, sentence, ss, ss_pos):
    split_func = {'?':split_question_mark,
                  '%':split_percent_sign,
                  '.':split_period_mark,
                  '/':split_foward_slash}
    if token in split_func:
        return split_func[token](sentence, ss, ss_pos)
    else:
        return ss, ss_pos

def apply_direction(token, sentence, ss, ss_pos):
    find_func = {'?':question_mark_apply_direction}
    if token in find_func:
        return find_func[token](sentence, ss, ss_pos)
    else:
        return ss, ss_pos

def question_mark_apply_direction(sentence, ss, ss_pos):
    for i in range(len(ss)):
        if ss[i][0]=='?':
            if i==0:
                ss[i]='?-right'
            elif i==len(ss)-1:
                ss[i]='?-left'
            else:
                left_distance = ss_pos[i][0]-ss_pos[i-1][1]+1
                right_distance = ss_pos[i+1][0]-ss_pos[i][1]+1
                if left_distance>right_distance:
                    ss[i]='?-right'
                else:
                    ss[i]='?-left'
    return ss, ss_pos




