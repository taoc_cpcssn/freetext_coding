import re
import pyodbc
import itertools
import pyodbc
from lxml import etree
import sys
import csv
from collections import Counter
from coding_util import split_token
from coding_util import apply_direction

EXCLUSION_SCOPE = 4

class RuleCoder(object):
    def __init__(self):
        self._split_pat = re.compile(r'[^a-zA-Z0-9\.\?\/\%\^]+', re.I)
        self._condition_map = {}
        self._diagnosises = []
        self._exclusion_terms = []
        self._icd10_to_icd9_map ={}
        self._icd9_code_to_text_map = {}
        self._icpc_to_icd9_map = {}

    def load(self, node):
        for child_node in node:
            condition = None
            diagnosis = None
            et = None

            if child_node.tag=='exclude':
                et = ExclusionTerm(self, child_node)
            elif child_node.tag == 'condition':
                condition_name = child_node.attrib['cond_name']
                condition = self.get_condition(condition_name)
                condition._late_init(self, child_node)
            elif child_node.tag == 'diagnosis':
                diagnosis = Diagnosis(self, child_node)

            if et is not None:
                self._exclusion_terms.append(et)

            if diagnosis is not None:
                for d in self._diagnosises:
                    if d.get_diagnosis_code() == diagnosis.get_diagnosis_code():
                        assert False
                self._diagnosises.append(diagnosis)

            if condition is not None:
                self._condition_map[condition.get_condition_name()] = condition

    def load_map(self, fin, map, fmt="{1}"):
        line_reader = csv.reader(fin, quotechar='"')
        next(line_reader)

        for v in line_reader:
            if len(v)==2:
                map[v[0]] = fmt.format(v[0],v[1])

    def load_icd10(self, icd10_to_icd9_map):
        self.load_map(icd10_to_icd9_map, self._icd10_to_icd9_map)

    def load_icd9(self, icd9_code_to_text_map):
        self.load_map(icd9_code_to_text_map, self._icd9_code_to_text_map, "{0}:{1}")

    def load_icpc(self, icpc_to_icd9_map):
        self.load_map(icpc_to_icd9_map, self._icpc_to_icd9_map)

    def get_condition(self, condition_name):
        if condition_name in self._condition_map:
            condition = self._condition_map[condition_name]
        else:
            condition = Condition(self, None)
            self._condition_map[condition_name]=condition
        return condition

    def code(self, s, code_type=None, code=None):
        code_type_func_map = {'ICD9': self.code_icd9,
                              'ICD10': self.code_icd10,
                              'ICPC': self.code_icpc,
                              'ICD(ONT)': self.code_icdont}

        finded_diagnosis_by_code = None
        if code_type is not None and code_type in code_type_func_map:
            finded_diagnosis_by_code = code_type_func_map[code_type](code)

        find_diagnosis = set()
        if s is not None:
            find_diagnosis = self.code_text(s)

        if ((finded_diagnosis_by_code is not None)
                and (finded_diagnosis_by_code not in find_diagnosis)
                and (finded_diagnosis_by_code[:3] not in ['250','401','491','492','496', '493'])):
            find_diagnosis.add(finded_diagnosis_by_code)

        return '|'.join(find_diagnosis)

    def code_icdont(self, code):
        if code in self._icd9_code_to_text_map:
            return self._icd9_code_to_text_map[code]
        else:
            return None

    def code_icd9(self, code):
        if code in self._icd9_code_to_text_map:
            return self._icd9_code_to_text_map[code]
        else:
            return None

    def code_icpc(self, code):
        if code in self._icpc_to_icd9_map:
            return self.code_icdont(self._icpc_to_icd9_map[code])
        else:
            return None

    def code_icd10(self, code):
        if code in self._icd10_to_icd9_map:
            return self.code_icd9(self._icd10_to_icd9_map[code])
        else:
            return None

    def code_text(self, s):
        find_diagnosis = set()
        s = [st1 for st in s.split('\n') for st1 in st.split('\r') if len(st1)>0]
        for st in s:
            tmp = [[m.start(0), m.end(0)] for m in self._split_pat.finditer(st)]
            if len(tmp)>0:
                ss_pos = [[0, tmp[0][0]]]
                for i in range(1, len(tmp)):
                    ss_pos.append([tmp[i-1][1], tmp[i][0]])
                ss_pos.append([tmp[-1][1], len(st)])
            else:
                ss_pos = [[0, len(st)]]

            ss_pos = [span for span in ss_pos if span[1]-span[0]>0]
            ss = [s1 for s1 in self._split_pat.split(st) if len(s1)>0]

            ss, ss_pos = split_token('?', st, ss, ss_pos)
            ss, ss_pos = split_token('%', st, ss, ss_pos)
            ss, ss_pos = split_token('.', st, ss, ss_pos)
            ss, ss_pos = split_token('/', st, ss, ss_pos)

            ss, ss_pos = apply_direction('?', st, ss, ss_pos)

            # print(ss, ss_pos)

            s = ' {0} '.format(' '.join(ss))
            # print(s)

            ss_spans = [[0,0] for _ in range(len(ss))]
            start = 1
            for i in range(len(ss)):
                ss_spans[i][0] = start
                ss_spans[i][1] = start + len(ss[i])
                start = ss_spans[i][1] + 1

            find_diagnosis |= {'{0}:{1}'.format(
                d.get_diagnosis_code(),
                d.get_diagnosis_name())
             for d in self._diagnosises
                if d.test(s, ss_spans, self._exclusion_terms)}
        return find_diagnosis

class RuleObject(object):
    def __init__(self, coder, node):
        self._rule_objects = []
        self._exclusion_terms = []
        if node is not None:
            self._build_nodes(coder, node)

    def _build_nodes(self, coder, node):
        for child_node in node:
            ro = None
            et = None
            if child_node.tag=='exclude':
                et = ExclusionTerm(coder, child_node)
            elif child_node.tag == 'rule':
                ro = Rule(coder, child_node)
            elif child_node.tag == 'rule-set':
                ro = RuleSet(coder, child_node)
            elif child_node.tag == 'condition':
                condition_name = child_node.attrib['cond_name']
                condition = coder.get_condition(condition_name)
                condition._late_init(coder, node)
            elif child_node.tag == 'diagnosis':
                ro = Diagnosis(coder, child_node)
            elif child_node.tag == 'include':
                ro = SearchTerm(coder, child_node)
            elif child_node.tag == 'include-condition':
                condition_name = child_node.attrib['cond_name']
                ro = coder.get_condition(condition_name)

            if ro is not None:
                self._rule_objects.append(ro)

            if et is not None:
                self._exclusion_terms.append(et)

    def test(self, s, ss_spans, exclusion_terms):
        pass

    def __repr__(self):
        pass

class SearchTerm(RuleObject):
    def __init__(self, coder, node):
        self._pat_str = node.attrib['item_value']
        self._pat = re.compile(' ({0}) '.format(self._pat_str), re.I)

    def test(self, s, ss_spans, exclusion_terms):
        finds = [[m.start(1), m.end(1)]
                    for m in self._pat.finditer(s)]

        if len(finds)>0:
            finds_ = sorted(list(itertools.chain.from_iterable(
                        [[(f[0],i,0), (f[1],i, 1)] for i,f in enumerate(finds)]
                        )), key=lambda v:v[0])
            finds_spans = [[0,0] for _ in range(len(finds))]

            pi = 0
            for i in range(len(finds_)):
                while finds_[i][0]>ss_spans[pi][1]:
                    pi+=1
                finds_spans[finds_[i][1]][finds_[i][2]]=pi

            for span in finds_spans:
                # print(span, ss_spans)
                pas = True
                for et in exclusion_terms:
                    if et.test(s, ss_spans, span):
                        pas = False
                        break
                if pas:
                    return pas
        return False

    def __repr__(self):
        return 'include {0}'.format(self._pat)

class ExclusionTerm(object):
    def __init__(self, coder, node):
        self._pat_str = node.attrib['item_value']
        self._wc = Counter(self._pat_str)[' ']+1
        self._pat = re.compile(' ({0}) '.format(self._pat_str), re.I)
        if 'apply_direction' not in node.attrib:
            self._direction = 'all'
        else:
            self._direction = node.attrib['apply_direction']

    def test(self, s, ss_spans, span):
        direction_selector = {'all': s[ss_spans[max([span[0]-EXCLUSION_SCOPE-self._wc,0])][0]-1:ss_spans[min([span[1]+EXCLUSION_SCOPE+self._wc,len(ss_spans)-1])][1]+1],
                              'right': s[ss_spans[max([span[0]-EXCLUSION_SCOPE-self._wc,0])][0]-1:ss_spans[span[1]][1]+1],
                              'left': s[ss_spans[span[0]][0]-1:ss_spans[min([span[1]+EXCLUSION_SCOPE+self._wc,len(ss_spans)-1])][1]+1]}
        s_search = direction_selector[self._direction]
        return self._pat.search(s_search) is not None

    def __repr__(self):
        return 'exclude {0} and apply to {1} direction'.format(self._pat_str, self._direction)

class Rule(RuleObject):
    def __init__(self, coder, node):
        RuleObject.__init__(self, coder, node)

    def test(self, s, ss_spans, exclusion_terms):
        for ro in self._rule_objects:
            if not ro.test(
                        s, ss_spans,
                        self._exclusion_terms+exclusion_terms):
                return False
        return True

    def __repr__(self):
        return 'rule'

class RuleSet(RuleObject):
    def __init__(self, coder, node):
        RuleObject.__init__(self, coder, node)

    def test(self, s, ss_spans, exclusion_terms):
        for ro in self._rule_objects:
            if ro.test(
                    s, ss_spans,
                    self._exclusion_terms+exclusion_terms):
                return True
        return False

    def __repr__(self):
        return 'rule-set'

class Condition(Rule):
    def __init__(self, coder, node):
        Rule.__init__(self, coder, node)
        if node is not None:
            self._condition_name = node.attrib['cond_name']
        else:
            self._condition_name = 'Wait intialization'

    def _late_init(self, coder, node):
        self._condition_name = node.attrib['cond_name']
        self._build_nodes(coder, node)

    def get_condition_name(self):
        return self._condition_name

    def __repr__(self):
        return 'Condition {0}'.format(self._condition_name)

class Diagnosis(Rule):
    def __init__(self, coder, node):
        Rule.__init__(self, coder, node)
        self._diagnosis_name = node.get('icd9_desc')
        self._diagnosis_code = node.get('icd9_code')

    def get_diagnosis_name(self):
        return self._diagnosis_name

    def get_diagnosis_code(self):
        return self._diagnosis_code

    def __repr__(self):
        return 'Diagnosis {}'.format(self._diagnosis_code)

conn_driver = 'DRIVER={SQL Server Native Client 11.0}'
conn_server = 'SERVER=DAC-REPOSITORY\\SQL2017'
conn_account = 'Trusted_Connection=yes'
database_name = 'N04_2019Q2v2'

def _create_conn(database_name):
    conn_db = 'DATABASE=' + database_name
    conn_str = ';'.join([conn_driver,conn_server,conn_db,conn_account])
    conn = pyodbc.connect(conn_str)
    return conn

def code_diagnosis(table_name, coder):
    cursor = _create_conn(database_name).cursor()
    sql = "UPDATE {0} SET DiagnosisCode_calc=NULL, DiagnosisCodeType_calc=NULL, DiagnosisText_calc=NULL".format(table_name)
    cursor.execute(sql)
    cursor.commit()

    sql = "SELECT {0}_ID, DiagnosisText_orig, DiagnosisCode_orig, DiagnosisCodeType_orig FROM {0}".format(table_name)
    cursor.execute(sql)

    update_sql_template = "UPDATE {0} SET DiagnosisCode_calc=?, DiagnosisCodeType_calc='ICD9', DiagnosisText_calc=?"
    update_sql_template+=' WHERE {0}_ID={1}'

    update_cursor = _create_conn(database_name).cursor()

    count = 0
    for row in cursor.fetchall():
        table_id = row[0]
        diagnosis_text_orig = row[1]
        diagnosis_code_orig = row[2]
        # diagnosis_codetype_orig = row[3].upper() if row[3] is not None else None
        diagnosis_codetype_orig = None
        r = coder.code(diagnosis_text_orig, diagnosis_codetype_orig, diagnosis_code_orig)
        if len(r.strip())>0:
            rvs = [rv.split(':') for rv in r.split('|')]
            diagnosis_code = ' ; '.join([rv[0] for rv in rvs])
            diagnosis_text = rvs[0][1]
            update_sql=update_sql_template.format(table_name, table_id)
            update_cursor.execute(
                update_sql,
                diagnosis_code, diagnosis_text[:250])
            update_cursor.commit()
        count+=1
        if count>0 and count%100==0:
            print("Processed ... {0}\r".format(count), end="", flush=True)

    cursor.close()
    update_cursor.close()

coder = RuleCoder()
with open("test2.xml") as fp:
    # open("icd9_code_to_text_map.csv") as fp_icd9, \
    # open("icd10_to_icd9_map.csv") as fp_icd10, \
    # open("icpc_to_icd9_map.csv") as fp_icpc:
    tree = etree.parse(fp)
    coder.load(tree.getroot())
    # coder.load_icd9(fp_icd9)
    # coder.load_icd10(fp_icd10)
    # coder.load_icpc(fp_icpc)

    table_names = ['HealthCondition_tao']

    # for tn in table_names:
    #     code_diagnosis(tn, coder)
    #     break

    print(coder.code("""T2DM 2002 /HTN /Microalbuminuria /Dyslipidemia /Hypothyroid /Colonoscopy(FHx) - 2008, 2013, 2019 - 5y/""", None, ""))
